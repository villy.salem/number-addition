# Number Addition
## Description
A web service that collects inputs from multiple requests and returns the sum of these inputs after an "end" request is received.  
Payloads for "add" requests must be integers greater than zero.  
Payloads for "end" requests can only contain the string "end".  

### Example valid requests:  
curl -X POST -H "Content-Type: text/plain" -d "5" localhost:1337  
curl -X POST -H "Content-Type: text/plain" -d "end" localhost:1337  

### Build:
mvn clean install  

### Run:
mvn spring-boot:run  

### Test:
mvn test  
