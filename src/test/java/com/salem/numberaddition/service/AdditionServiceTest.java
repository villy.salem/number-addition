package com.salem.numberaddition.service;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.stream.LongStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AdditionServiceTest {
	// Determines the number of parallel requests that will be created. Request payload values will range from 0-RANGE.
	private static final int RANGE = 20;

	private final AdditionService additionService = new AdditionService();
	private final List<Long> responses = new ArrayList<>();

	@Test
	void testAddition() throws InterruptedException {
		final CountDownLatch latch = new CountDownLatch(RANGE);
		LongStream.range(0, RANGE).parallel().forEach(input -> executeAddRequest(input, latch));

		// Wait for addition requests to reach synchronization block (not guaranteed)
		Thread.sleep(1000);
		addResponse(additionService.end());
		latch.await();

		// Assert that number of responses is equals to the number of addition requests + the end request
		assertThat(responses.size()).isEqualTo(RANGE + 1);
		// Assert that all responses contain the same value
		assertThat(new HashSet<>(responses).size()).isEqualTo(1);
		// Assert that the value contained in the responses is the expected total of all the addition requests
		assertTrue(responses.contains(LongStream.range(0, RANGE).sum()));
	}

	private void executeAddRequest(final long input, final CountDownLatch latch) {
		new Thread(() -> {
			try {
				addResponse(additionService.add(input));
				latch.countDown();
			} catch (InterruptedException ignored) {
			}
		}).start();
	}

	private synchronized void addResponse(final long response) {
		responses.add(response);
	}
}
