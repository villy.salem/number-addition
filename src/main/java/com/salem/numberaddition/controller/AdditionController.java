package com.salem.numberaddition.controller;

import com.salem.numberaddition.service.AdditionService;
import com.salem.numberaddition.validator.RequestValidator;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AdditionController {
	private static final Logger LOG = LoggerFactory.getLogger(AdditionController.class);

	private final AdditionService additionService;

	@RequestMapping(path = "/", consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
	public String index(@RequestBody final String body) throws InterruptedException {
		RequestValidator.validateRequest(body);

		if (RequestValidator.isEndRequest(body)) {
			return formatResponse(additionService.end());
		}
		return formatResponse(additionService.add(Long.parseLong(body)));
	}

	@ExceptionHandler({Exception.class})
	public String handleException(final Exception e) {
		LOG.error("Exception occurred", e);
		return String.format("Exception occurred: %s", e.getMessage());
	}

	private String formatResponse(final long response) {
		return String.valueOf(response);
	}
}