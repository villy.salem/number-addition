package com.salem.numberaddition.model;

import java.util.concurrent.atomic.AtomicLong;

public class InputTotal {
	private AtomicLong runningTotal = new AtomicLong(0L);
	private long previousTotal;

	/**
	 * Add input value to current total value, wait for end request.
	 *
	 * @param input input value
	 * @return total value
	 * @throws InterruptedException InterruptedException
	 */
	public long add(final long input) throws InterruptedException {
		runningTotal.addAndGet(input);
		return getTotalWaiting();
	}

	/**
	 * Reset running total and notify waiting threads.
	 *
	 * @return total value
	 */
	public synchronized long end() {
		long previousTotal = runningTotal.getAndSet(0L);
		this.previousTotal = previousTotal;

		notifyAll();

		return previousTotal;
	}

	/**
	 * Wait for end request, then return the total value.
	 *
	 * @return total value
	 * @throws InterruptedException InterruptedException
	 */
	private synchronized long getTotalWaiting() throws InterruptedException {
		while (runningTotal.get() > 0) {
			wait();
		}
		return previousTotal;
	}
}
