package com.salem.numberaddition.service;

import com.salem.numberaddition.model.InputTotal;
import org.springframework.stereotype.Service;

@Service
public class AdditionService {
	private final InputTotal inputTotal = new InputTotal();

	public long add(final long input) throws InterruptedException {
		return inputTotal.add(input);
	}

	public long end() {
		return inputTotal.end();
	}
}
