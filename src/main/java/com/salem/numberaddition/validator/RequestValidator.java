package com.salem.numberaddition.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

public class RequestValidator {
	private static final String REQUEST_BODY_END = "end";
	private static final String INVALID_INPUT_RESPONSE = "Invalid input: %s";

	public static void validateRequest(final String body) {
		Assert.isTrue(isEndRequest(body) || isAdditionRequest(body), String.format(INVALID_INPUT_RESPONSE, body));
	}

	public static boolean isEndRequest(final String body) {
		return REQUEST_BODY_END.equalsIgnoreCase(body);
	}

	public static boolean isAdditionRequest(final String body) {
		return StringUtils.isNumeric(body) && Long.parseLong(body) > 0;
	}
}
