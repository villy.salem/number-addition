package com.salem.numberaddition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumberAdditionApplication {
	public static void main(String[] args) {
		SpringApplication.run(NumberAdditionApplication.class, args);
	}
}
